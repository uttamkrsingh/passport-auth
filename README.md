# Boilerplate

1. Expressjs
2. Mongodb with Mongoose
3. Passport Local Authentication
4. Controller and Routes
5. Middlewares
6. HBS Template Engine
7. Helment Middleware configured in @softcripto/express
8. CORS Middleware it should be configured in application as per requirement

## .env and Environment variables

Declare all **Environment variables** in `./@types/environment.d.ts` file in _ProcessEnv_ interface

# Reference

- [connect-ensure-login](https://github.com/jaredhanson/connect-ensure-login)
- [Local Authentication using passport](https://www.sitepoint.com/local-authentication-using-passport-node-js/)
- [Mongoose Plugin to Handle authentication, user register, login etc.](https://www.npmjs.com/package/passport-local-mongoose)
- [Validator.js Validate String](https://www.npmjs.com/package/validator)
- []()

# VSCode requirements

- Install ESlint and Prettier

# Files and Folders

- `src/server.ts` main file to run and configure server
- `src/api` Restful API controllers
- `src/controllers` Web route controllers
- `src/lib` Server related libraries.
- `src/services` Services to handle business logics {Don't modify unless you know what are you doing stuffs}
- `src/model` Database (mongoose) models
- `src/middlewares` Route middleware
- `src/interface` Typescript interface declration
