# To-dos

- Morgan Logging
- JWT token
- Validate Environment variables before running application (Rohan)
- Findout code coverage and usefullness

# Features

- Model Validation
- Cors
- robots.txt
- End to End test
- Unit Test
- Linting
- Google Login
- Facebook login
- User Login, logout
- Move database Service from @softcripto/express to within application
- @softcripto/express
- typescript
- session
- mongodb/mongoose
- passport local strategy
- route
- controller
- write bin/www in typescript and move code to src/server.ts
