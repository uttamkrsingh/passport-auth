import { expect } from "chai";
import { Author } from "../../src/models";
/*
 * This file runs
 * before() and after() root level hooks.
 * before() hook connects to database
 * after() hook deletes all collections and drops database
 *
 * Important: must be imported
 */
import "./_hook.ignore";

describe("Model", function () {
  describe("Author", function () {
    it("Should save new author", async function () {
      // Create an author
      const author = await Author.create({
        name: "Akash Pithakote Thapa",
        address: "Chalouni Tea Garden",
        birthday: "August 27 1980",
        language: ["Nepali"],
      });
      expect(author.name).to.equal("Akash Pithakote Thapa");
      expect(author.address).to.equal("Chalouni Tea Garden");
      expect(author.birthday).to.equal("August 27 1980");
      expect(author.language).to.be.an("array").to.be.lengthOf(1);
      expect(author.language[0]).to.be.a("string").and.equal("Nepali");
    });

    it("Should not save invalid author", async function () {
      try {
        // Create an author
        const author = await Author.create({
          name: "A",
          address: "",
          birthday: "",
          language: [""],
        });
        expect(author).to.be.null;
      } catch (e) {
        expect(e.errors.name.message).to.equal(
          "Name must be alphbets and more than 2 characters long."
        );
      }
    });
  });
});
