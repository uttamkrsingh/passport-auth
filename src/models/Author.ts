import { model, Schema } from "mongoose";
import { IAuthorModel } from "../interface";
import validate from "validator";

/// author schema
const AuthorSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  birthday: {
    type: String,
    required: true,
  },
  language: [],
});

AuthorSchema.path("name").validate(function (v: string) {
  return v.length > 2;
}, "Name must be alphbets and more than 2 characters long.");

export const Author = model<IAuthorModel>("Author", AuthorSchema);
