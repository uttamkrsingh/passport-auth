/**
 * Run root level hook to connect database
 * and dissconent when all test completed.
 *
 * NOTE:
 * Do not modify this file unless you know what you are doing
 *
 * @packageDescription
 */

import database from "../lib/_database";
import mongoose from "mongoose";

before(function (done) {
  database("mongodb://localhost:27017/test", "test")
    .then(() => {
      done();
    })
    .catch(done);
});
after(function (done) {
  mongoose.connection.db.dropDatabase(function (err) {
    if (err) return done(err);
    mongoose
      .disconnect()
      .then(function () {
        done();
      })
      .catch(done);
  });
});
