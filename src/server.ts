/**
 * Main file to configure and run express server
 *
 * @packageDescription
 */

require("dotenv").config();
import passport from "passport";
import { Server } from "@softcripto/express";
import { Application } from "express";
import * as Auth from "./lib/Auth";
import { runServer } from "./lib/www";
import database from "./lib/_database";
import session from "./lib/_session";
import { beforeAllRoute } from "./middlewares/beforeAllRoute";
const debug = require("debug")("app");
const robots = require("express-robots-txt");

// load all controllers that handles http request
import "./controllers";

debug("Database connection started.");

/*
 * Connecting to database
 * When connected configure server
 */
database(process.env.DATABASE as string, process.env.NODE_ENV as string)
  .then(() => {
    debug("Database connection success.");

    // Initialising Express App with @softcripto/express
    const app: Application = Server.create({
      views: "views",
      public: "public",

      // Run code just before Route mount
      beforeRouteInjection: function (app: Application) {
        if (process.env.NODE_ENV != "production") {
          app.use(robots({ UserAgent: "*", Disallow: "/" }));
        }

        // Set session and flash middleware
        session(app, process.env.SESSION_SECRET as string);

        /*
         * Passport Local Strategy
         * User can login with username and password
         */
        Auth.local();

        // Facebook Login
        // Auth.Facebook.setStrategy(
        //   process.env.FACEBOOK_CLIENT_ID as string,
        //   process.env.FACEBOOK_CLIENT_SECRET as string,
        //   process.env.FACEBOOK_CALLBACK_URL as string
        // );

        // Google Login
        // Auth.Google.setStrategy(
        //   process.env.GOOGLE_CLIENT_ID as string,
        //   process.env.GOOGLE_CLIENT_SECRET as string,
        //   process.env.GOOGLE_CALLBACK_URL as string
        // );

        // Initialise passport
        app.use(passport.initialize());
        app.use(passport.session());

        // Add middleware before all route
        app.all("*", beforeAllRoute);

        // Google and Facebook Auth routes
        // Auth.Google.setRoute(app);
        // Auth.Facebook.setRoute(app);
      },
    });

    // Run server
    runServer(app, process.env.PORT as string);
  })
  .catch((err) => {
    console.log(err);
    process.exit(1); // Doesn't run server
  });
