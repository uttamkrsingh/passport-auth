/**
 * Authenticate request using passport's local strategy
 *
 * @packageDocumentation
 */

import passport from "passport";
import { User } from "../../models";

/// This package is handling Passport Local Strategy
/// https://www.npmjs.com/package/passport-local-mongoose
export function local() {
  passport.use(User.createStrategy());
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());
}
