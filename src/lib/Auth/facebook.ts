import { Application } from "express";
import passport from "passport";
import facebook from "passport-facebook";
import { verifyUser } from "./verifyUser";

/// Set Route
export function setRoute(app: Application) {
  app.get(
    "/auth/facebook",
    passport.authenticate("facebook", {
      scope: "email",
    })
  );
  app.get(
    "/callback/facebook",
    passport.authenticate("facebook", {
      successRedirect: "/",
      failureRedirect: "/login",
    })
  );
}

/// Facbook strategy
export function setStrategy(
  clientID: string,
  clientSecret: string,
  callbackURL: string
) {
  passport.use(
    new facebook.Strategy(
      {
        clientID,
        clientSecret,
        callbackURL,
      },
      verifyUser
    )
  );
}
