import supertest from "supertest";

const request = supertest("http://localhost:5555");

describe("GET /", function () {
  it("responds with json", function (done) {
    request.get("/").expect(200, done);
  });
});
