import { NextFunction, Request, Response } from "express";
import { Controller, Get, IRouteParams, Post } from "@softcripto/express";
import passport from "passport";
import { User } from "../models";
import { generateToken } from "../services/tokens";
import { sendMail } from '../services/mail';
import { checkPassword } from '../services/passwordValidator';

@Controller("/")
export default class RootController {
  static children: Array<IRouteParams> = []; /// required
  /// Home page
  @Get("/")
  async index(req: Request, res: Response, next: NextFunction) {
    try {
      res.locals.successMessage = req.flash("success");
      res.locals.errorMessage = req.flash("error");
      res.render("root/home");
    } catch (error) {
      next(error);
    }
  }
  /// Demo contact page
  @Get("/contact")
  one(req: Request, res: Response) {
    res.render("root/contact");
  }
  /// User Login where user can enter credentials
  /// Or Use third party login Facebook, Google etc.
  @Get("/login")
  loginForm(req: Request, res: Response) {
    if (req.isAuthenticated()) {
      return res.redirect("/");
    }
    res.locals.message = req.query.info; /// query string
    res.render("user/login", { layout: "guest" });
  
  }

  /// Handle User Login submission
  /// Authenticate using local strategy
  @Post("/login")
   login(req: Request, res: Response, next: NextFunction) {
    passport.authenticate("local", (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res.redirect("/login?info=" + info);
      }
         
      req.logIn(user, function (err) {
        
        if (err) {
          return next(err);
        }
        
        if(user.createdAt.getTime() === user.updatedAt.getTime()){
          return res.redirect('/changePassword')
        }
        return res.redirect(req.session?.returnTo || "/");
      });
    })(req, res, next);
  }

  /// Logout user
  @Get("/logout")
  logout(req: Request, res: Response) {
    req.logOut();
    res.redirect("/");
  }

/**
 * Signup page.
 * @route GET /signup
 */
  @Get("/signup")
  signupForm(req: Request, res: Response){
    res.locals.successMessage = req.flash("success");
    res.locals.errorMessage = req.flash("error");
    if (req.user) {
      return res.redirect("/");
    }
    res.render("user/signup", { message: "Create a new account"});
  }
/**
 * Create a new local account.
 * @route POST /signup
 */
  

  @Post("/signup")
   async signup(req: Request, res: Response, next: NextFunction){
      try {
          const password = await checkPassword(req.body.password);
          const user = await User.register(new User({username: req.body.username}), password);
          req.logIn(user, (err) => {
            if(err){return next(err)}
            req.flash("success", "New account created successfully.")
            res.redirect("/")
          })
          
        } catch (error) {
          req.flash("error", error.message);
          res.redirect(req.originalUrl);
        }
    }


  // Password reset
  @Get("/reset/:token")
  async resetForm(req: Request, res: Response, next: NextFunction){
    res.locals.successMessage = req.flash("success");
    res.locals.errorMessage = req.flash("error");
      if (req.isAuthenticated()) {
        return res.redirect("/");
      }
    try {
      const token = req.params.token.split('_');
      const user = await  User.findOne({_id: token[0]});

      if(user?.profile?.access_token != token[1]){
        req.flash("error",  "Password reset token is invalid." );
          return res.redirect("/forget");
      }
      if(user?.profile.refresh_token < ""+Date.now()){
        req.flash("error",  "Password reset token has expired." );
          return res.redirect("/forget");
      }
      const tokenDetails = {
        username: user.username,
        token: req.params.token
      }
       res.render("user/resetPassword", {tokenDetails});
    } catch (error) {
      next(error);
    }

  }

  // Reset password
  @Post("/reset/:token")
   async reset(req: Request, res: Response, next:NextFunction){
    if (req.isAuthenticated()) {
      return res.redirect("/");
    }
    try {
        const token = req.params.token.split('_');
        const user = await  User.findOne({_id: token[0]});

        if(!user){
          req.flash("error",  "Password reset token is invalid." );
            return res.redirect("/forget");
        }
        if(user?.profile?.access_token != token[1]){
          req.flash("error",  "Password reset token is invalid." );
            return res.redirect("/forget");
        }
        if(+user?.profile.refresh_token < Date.now()){
          req.flash("error",  "Password reset token has expired." );
            return res.redirect("/forget");
        }
        if(req.body.password != req.body.c_password){
          req.flash("error",  "Confirm password did not match" );
          return res.redirect(`/reset/${req.params.token}`)
        }
        if(user){
          const password = await checkPassword(req.body.password);
          await user.setPassword(password);
          await user.save();
          req.flash("success",  "Password reset successfully" );
          res.redirect('/logout');
        }
      } catch (error) {
        req.flash("error", error.message);
        res.redirect(req.originalUrl);
      }
    }

// Password forgot
  @Get("/forget")
  forgotForm(req: Request, res: Response){
    res.locals.successMessage = req.flash("success");
    res.locals.errorMessage = req.flash("error");
    res.render("user/forgetpassword", {userDetails: req.user});
  }

  // Password forgot
  @Post("/forget")
  async forgot(req: Request, res: Response, next: NextFunction){
    try {
        const user = await User.findOne({username: req.body.username});
        if(!user){
          req.flash("error", "User not found");
          res.redirect('/forget')
        }
        
        const tokenPayload = {
          body: req.body.username,
        }
        const token =  generateToken(tokenPayload);
        const mailDetails = {
          to: req.body.username,
          from: 'uttam@softcripto.com',
          subject: 'Reset password',
          text: 'Click on the link',
          html: `<a href=http://${req.headers.host}/reset/${user?._id}_${token}\n\n> ckeck hear to reset your passsword.</a>`
        }
        sendMail(mailDetails);

        const tokenExpireTime =  Date.now() + 3600000; // +1 hour
        const upProfile = {
          id: user?._id,
          access_token: token,
          refresh_token: ""+ tokenExpireTime,
          provider: "admin",
          emails: req.body.username,
        }

        await User.updateOne({ _id: user?._id }, { $set: { profile: upProfile } });
  
        req.flash("success", "Please check your email for further instructions.");
        res.redirect('/')
      } catch (error) {
        req.flash("error", error.message);
        res.redirect(req.originalUrl);
      }
  }

  // Password forgot
  @Get("/changePassword",)
  changeForm(req: Request, res: Response){
    res.locals.successMessage = req.flash("success");
    res.locals.errorMessage = req.flash("error");
    if (!req.isAuthenticated()) {
      return res.redirect("/");
    }
    
    res.render("user/changePassword", { userDetails: req.user });
  }

  @Post("/changePassword")
  async change(req: Request, res: Response, next: NextFunction){
    try {
      if (!req.isAuthenticated()) {
        return res.redirect("/");
      }
      if(req.body.password != req.body.c_password){
        return res.render('user/changePassword', {userDetails: req.user, message:'confirm Password not match'} )
      }
      const user = await User.findOne({username: req.body.username});
      if(user){
        const password = await checkPassword(req.body.password);
          await user.changePassword(req.body.old_password, password);
          await user.save();
            req.logIn(user, (err) => {
              if(err){return next(err)};
              req.flash("success", "Password change successfully.");
              res.redirect(req.originalUrl);
            })
        }
      }
    catch (error) {
      req.flash("error", error.message);
      res.redirect(req.originalUrl);
    }
   
  }
}
