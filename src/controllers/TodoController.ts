import { NextFunction, Request, Response } from "express";
import { Controller, Get, IRouteParams, Post } from "@softcripto/express";
import { Todo } from "../models";
import { TodoService } from "../services/TodoService";
import { ITodo } from "../interface";
import { ensureLoggedIn } from "connect-ensure-login";

interface IRequestTodoForm extends Request {
  body: ITodo;
}

// @Controller('/todos')
@Controller("/todos", ensureLoggedIn("/login"))
export default class TodoController {
  static children: Array<IRouteParams> = []; /// required
  /**
   * List all todos.
   */
  @Get("/")
  async index(req: Request, res: Response, next: NextFunction) {
    try {
      res.locals.successMessage = req.flash("success");
      res.locals.errorMessage = req.flash("error");
      const todos = await Todo.find().sort("-createdAt");
      res.render("todos/index", { todos: TodoService.init(todos) });
    } catch (error) {
      next(error);
    }
  }
  /**
   * Render Form to create new todo.
   */
  @Get("/create")
  async createForm(req: Request, res: Response, next: NextFunction) {
    try {
      res.locals.errorMessage = req.flash("error");
      res.render("todos/create");
    } catch (error) {
      next(error);
    }
  }

  /**
   * Handles New Todo
   *
   */
  @Post("/create")
  async create(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const exists = await Todo.findOne({ title: req.body.title });
      if (exists) {
        req.flash("error", "Same title already exists.");
        res.redirect(req.originalUrl);
        return;
      }
      await Todo.create(req.body);
      req.flash("success", "New Task created successfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }

  @Get("/delete/:id")
  async removeTodo(req: Request, res: Response, next: NextFunction) {
    try {
      const exists = await Todo.findById(req.params.id);
      if (!exists) {
        req.flash("error", "Invalid todo. Could not be removed.");
        res.redirect("/todos");
        return;
      }
      await Todo.remove({ _id: req.params.id });

      req.flash("success", "Task deleted succuessfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }
  /**
   * Update a Todo
   *
   */
  @Get("/update/:id")
  async updateForm(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        req.flash("error", "Invalid todo id.");
        res.redirect("/todos");
        return;
      }
      res.locals.todo = todo;
      res.render("todos/edit");
    } catch (error) {
      next(error);
    }
  }
  /**
   * Update
   */
  @Post("/update/:id")
  async update(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        req.flash("error", "Invalid todo id.");
        res.redirect("/todos");
        return;
      }
      await todo.update({ title: req.body.title });
      req.flash("success", "Task updated successfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }
}
