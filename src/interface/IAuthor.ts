import { Document } from "mongoose";

export interface IAuthor {
  name: string;
  address: string;
  birthday: string;
  language: Array<string>;
}

/// static method to be declared here
export interface IAuthorModel extends IAuthor, Document {}
