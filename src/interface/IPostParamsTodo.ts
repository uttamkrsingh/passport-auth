import { Request } from "express";
import { ITodo } from "./ITodo";

export interface IPostParamsTodo extends Request {
  body: ITodo;
}
