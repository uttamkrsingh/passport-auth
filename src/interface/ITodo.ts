import { Document } from "mongoose";

export interface ITodo {
  title: string;
  hasCompleted?: boolean;
  createdAt: Date;
}

export interface ITodoModel extends ITodo, Document {}
