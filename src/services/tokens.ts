import crypto from "crypto";

interface ItokenPayload  {
    body: string;
  }

export const generateToken = function (payload: ItokenPayload) {
    
    const hash =  crypto.createHmac('sha256', payload.body)
    .digest('hex');
    
    return hash;    
}