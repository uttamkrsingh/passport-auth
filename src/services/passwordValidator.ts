import validator from "validator";

export const checkPassword = function(password:string){
    if(!validator.isLength(password , {min: +process.env.passwordMinLength!, max: +process.env.passwordMaxLength!})){
        return Promise.reject(new Error(`Password must be between ${+process.env.passwordMinLength!} and ${+process.env.passwordMaxLength!} characters.`));
    };
    if(validator.isAlpha(password) || validator.isNumeric(password) || validator.isAlphanumeric(password) || (!validator.isAlphanumeric(password) && validator.isUppercase(password)) || (!validator.isAlphanumeric(password) && validator.isLowercase(password))){
        return Promise.reject(new Error('Password must contain at least one uppercase (ex: A, B, etc.), one lowercase letter, one digit (ex: 0, 1, 2, 3, etc.) and one special character -for example: $, #, @, !,%,^,&,*,(,) '))
    }
    return Promise.resolve(password);
}