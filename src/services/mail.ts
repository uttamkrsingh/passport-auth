const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

interface ImailDetails{
  to: string;
  from: string;
  subject: string;
  text?: string;
  html?:string
}

export const sendMail = async function (mailDetails: ImailDetails) {
    const msg = {
        to: mailDetails.to, // Change to your recipient
        from: mailDetails.from, // Change to your verified sender
        subject: mailDetails.subject,
        text: mailDetails.text,
        html: mailDetails.html,
      }
      sgMail
        .send(msg)
        .then(() => {
          console.log('Email sent')
        })
}